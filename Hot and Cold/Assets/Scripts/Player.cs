﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public GameObject lose_panel;
    public Text health_txt;
    public float speed;
    private float input;

    Rigidbody2D rb;
    Animator anim;
    AudioSource source;

    public int health;

    public float start_dash;
    private float dash_time;
    public float extra_speed;
    public bool is_dashing;

    void Start()
    {
        source = GetComponent<AudioSource>();
        health_txt.text = health.ToString();
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {

        if (input !=0)
        {
            anim.SetBool("is_running", true);
        } else
        {
            anim.SetBool("is_running", false);
        }
        if(input > 0)
        {
            transform.eulerAngles = new Vector3(0, 0, 0);
        }else if (input < 0)
        {
            transform.eulerAngles = new Vector3(0, 180, 0);
        }
        if (Input.GetKeyDown(KeyCode.Z) && is_dashing == false)
        {
            print("foi");
            speed += extra_speed;
            is_dashing = true;
            dash_time = start_dash;
        }
        if (dash_time <= 0 && is_dashing == true)
        {
            is_dashing = false;
            speed -= extra_speed;
        }
        else
        {
            dash_time -= Time.deltaTime;
        }
    }

    void FixedUpdate()
    {
        input = Input.GetAxisRaw("Horizontal") * speed;
        rb.velocity = new Vector2(input, rb.velocity.y);
    }
    
    public void TakeDamage(int dmg_amount)
    {
        source.Play();
        health -= dmg_amount;
        health_txt.text = health.ToString();

        if (health <= 0 )
        {
            lose_panel.SetActive(true);
            Destroy(gameObject);
        }
    }
}
