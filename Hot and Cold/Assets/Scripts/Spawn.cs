﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{
    public Transform[] spawners;
    public GameObject[] hazards;

    private float time_btw_spawns;
    public float start_time_spawn;

    public float 
           min_time_btw_spawns,
           decrease;
    public GameObject player;

    // Update is called once per frame
    void Update()
    {
        if (player != null)
        {
            if (time_btw_spawns <= 0)
            {
                Transform random_spawn = spawners[Random.Range(0, spawners.Length)];
                GameObject random_hazard = hazards[Random.Range(0, hazards.Length)];

                Instantiate(random_hazard, random_spawn.position, Quaternion.identity);
                if (start_time_spawn > min_time_btw_spawns)
                {
                    start_time_spawn -= decrease;
                }

                time_btw_spawns = start_time_spawn;
            }
            else
            {
                time_btw_spawns -= Time.deltaTime;
            }
        }

    }
}
