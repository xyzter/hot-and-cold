﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomAudio : MonoBehaviour
{
    private AudioSource source;
    public AudioClip[] sounds;

    void Start()
    {
        source = GetComponent<AudioSource>();
        int rand_sound = Random.Range(0, sounds.Length);
        source.clip = sounds[rand_sound];
        source.Play();
    }
    
}
