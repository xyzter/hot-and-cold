﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float
        min_speed,
        max_speed;

    float speed;

    Player player;

    public int damage;
    public GameObject[] explosions;
    int random_explosion;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("player").GetComponent<Player>();
        speed = Random.Range(min_speed, max_speed);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector2.down * speed * Time.deltaTime);
    }
    void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "player")
        {
            random_explosion = (int)Random.Range(0, explosions.Length);
            player.TakeDamage(damage);
            Instantiate(explosions[random_explosion], transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
        if(collision.tag == "Ground")
        {
            random_explosion = (int)Random.Range(0, explosions.Length);
            Instantiate(explosions[random_explosion], transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
