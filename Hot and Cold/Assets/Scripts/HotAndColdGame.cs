﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HotAndColdGame : MonoBehaviour
{
    int random_number;
    [Range(0, 100)]
    public int guess;

    int number_of_attempts;
    // Start is called before the first frame update
    void Start()
    {
        random_number = Random.Range(0, 101);
        print("Welcome to the Hot & Cold number guessing game.");
        print("A random number between 0 and 100 has been chosen. Your goal is to guess that number in the least attemps");
        print("Enter in your guess and press the space bar to submit it. Good Luck!");
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown("space"))
        {
            number_of_attempts += 1;
            if (guess == random_number)
            {
                print("Congratulations! You have won and it only took " + number_of_attempts.ToString() + "attempts");
            }
            else if (Mathf.Abs(random_number - guess) <= 10)
            {
                print("Boiling.");
            }
            else if (Mathf.Abs(random_number - guess) <= 20)
            {
                print("Very Hot.");
            }
            else if (Mathf.Abs(random_number - guess) <= 30)
            {
                print("Hot.");
            }
            else if (Mathf.Abs(random_number - guess) <= 40)
            {
                print("Quite hot.");
            }
            else if (Mathf.Abs(random_number - guess) <= 50)
            {
                print("Neutral.");
            }
            else if (Mathf.Abs(random_number - guess) <= 60)
            {
                print("Quite cold.");
            }
            else if (Mathf.Abs(random_number - guess) <= 70)
            {
                print("Cold.");
            }
            else if (Mathf.Abs(random_number - guess) <= 80)
            {
                print("Very Cold.");
            }
            else if (Mathf.Abs(random_number - guess) <= 100)
            {
                print("Freezing.");
            }
        }
        
    }
}
